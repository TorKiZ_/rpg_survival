using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CraftingSystem : MonoBehaviour
{
    [SerializeField]
    public RecipeData[] availableRecipes;

    [SerializeField]
    private GameObject recipeUIPrefab;

    [SerializeField]
    private Transform recipeParent;

    [SerializeField] 
    private MoveBehaviour moveBehaviour;

    [SerializeField]
    private KeyCode openCraftPanelInput;

    [SerializeField]
    public GameObject craftingPanel;

    void Start()
    {
        UpdateDisplayedRecipes();
    }

    private void Update()
    {
        if(Input.GetKeyDown(openCraftPanelInput))
        {
            craftingPanel.SetActive(!craftingPanel.activeSelf);
            UpdateDisplayedRecipes();
        }
    }
    public void UpdateDisplayedRecipes()
    {
        foreach(Transform child in recipeParent)
        {
            Destroy(child.gameObject);
        }

        for (int i = 0; i < availableRecipes.Length; i++)
        {
           GameObject currentRecipe = Instantiate(recipeUIPrefab, recipeParent);
           currentRecipe.GetComponent<Recipe>().Configure(availableRecipes[i]);
        }
    }

 /*   public void AddRecipe(ItemData itemCurrentlySelected)
    {
        availableRecipes.Append(itemCurrentlySelected.recipeData);
        UpdateDisplayedRecipes();
    } 
 */
}
