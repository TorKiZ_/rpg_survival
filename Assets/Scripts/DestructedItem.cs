using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructedItem : MonoBehaviour
{
    [SerializeField]
    private GameObject cellsParents;

    [SerializeField]
    private GameObject visual;

    [SerializeField]
    private AudioSource audioBreak;

    [SerializeField]
    private ItemsToDrop[] itemsToDrop;

    [SerializeField]
    private float timeToActiveCells = 4f;

    private Vector3 spawnItemOffset = new Vector3(0, 0.5f, 0);
    public void DestroyItem()
    {
        StartCoroutine(CellsParentActiveTime(timeToActiveCells));
    }

    IEnumerator CellsParentActiveTime(float timeToActiveCells)
    {
        while (cellsParents)
        {
            //Anim time
            yield return new WaitForSeconds(0.4f);

            MeshCollider parentMCollider = GetComponent<MeshCollider>();
            BoxCollider parentBCollider = GetComponent<BoxCollider>();

            Destroy(visual);
            gameObject.tag = "Untagged";
            gameObject.layer = 0;

            for (int i = 0; i < itemsToDrop.Length; i++)
            {
                ItemsToDrop itemToDrop = itemsToDrop[i];
                if (Random.Range(1, 101) <= itemToDrop.dropChance)
                {
                    GameObject instantiatedItem = Instantiate(itemToDrop.itemData.prefab);
                    instantiatedItem.transform.position = gameObject.transform.position + spawnItemOffset;
                }
            }
            audioBreak.Play();
            cellsParents.gameObject.SetActive(true);

            yield return new WaitForSeconds(1f);
            Destroy(parentBCollider);
            Destroy(parentMCollider);

            yield return new WaitForSeconds(timeToActiveCells);

            cellsParents.SetActive(false);
            Destroy(gameObject);
        }
    }
}

[System.Serializable]
public class ItemsToDrop
{
    public ItemData itemData;

    [Range(0, 100)]
    public int dropChance;
}
