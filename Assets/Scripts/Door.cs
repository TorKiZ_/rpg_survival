using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public AudioSource openDoor;

    [SerializeField]
    private float rotationY;

    [HideInInspector]
    public bool isOpened = false;

    public void OpenDoor()
    {
            transform.Rotate(new Vector3(0, rotationY, 0));
    }

    public void CloseDoor()
    {
        transform.Rotate(new Vector3(0, 0, 0));
    }
}
