using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseChestPanel : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ChestSystem.instance.CloseChest();
        }
    }
}
