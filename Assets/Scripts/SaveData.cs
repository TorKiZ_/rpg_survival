using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SaveData : MonoBehaviour
{
    private SaveToJsonData saveToJsonData = new SaveToJsonData();

    [SerializeField]
    private GameObject saveGO;
    [SerializeField]
    private Text saveText;

    [SerializeField]
    private GameObject loadGO;
    [SerializeField]
    private Text loadText;

    private bool openSaveMessage = false;
    private bool openLoadMessage = false;

    public static SaveData instance;

    private void Awake()
    {
        instance= this;
    }

    public void SaveToJson()
    {
        if (openLoadMessage) { return; }
        if (openSaveMessage) { return; }

        StartCoroutine(OpenSaveMessage());
        saveToJsonData.content = Inventory.instance.content;
        saveToJsonData.currentGold = Inventory.instance.currentGold;
        saveToJsonData.currentGems = Inventory.instance.currentGems;
        saveToJsonData.currentHealth = PlayerStats.instance.currentHealth;
        saveToJsonData.currentHunger = PlayerStats.instance.currentHunger;
        saveToJsonData.currentThirst = PlayerStats.instance.currentThirst;
        saveToJsonData.currentLevel = PlayerStats.instance.currentExperience;
        string inventoryData = JsonUtility.ToJson(saveToJsonData);
        string filePath = Application.persistentDataPath + "/InventoryData.json";
        System.IO.File.WriteAllText(filePath, inventoryData);
    }

    public void LoadJson()
    {
        if(openLoadMessage) { return; }
        if (openSaveMessage) { return; }

        StartCoroutine(OpenLoadMessage());
        string filePath = Application.persistentDataPath + "/InventoryData.json";
        string inventoryData = System.IO.File.ReadAllText(filePath);

        saveToJsonData = JsonUtility.FromJson<SaveToJsonData>(inventoryData);
        Debug.Log(saveToJsonData.currentGold);
        Inventory.instance.content = saveToJsonData.content;
        Inventory.instance.currentGold = saveToJsonData.currentGold;
        Inventory.instance.currentGems = saveToJsonData.currentGems;
        PlayerStats.instance.currentHealth = saveToJsonData.currentHealth;
        PlayerStats.instance.currentHunger = saveToJsonData.currentHunger;
        PlayerStats.instance.currentThirst = saveToJsonData.currentThirst;
        PlayerStats.instance.currentExperience = saveToJsonData.currentLevel;
    }
    IEnumerator OpenSaveMessage()
    {
            openSaveMessage = true;
            saveGO.SetActive(true);
            yield return new WaitForSeconds(1f);
            saveText.text += ".";
            yield return new WaitForSeconds(1f);
            saveText.text += ".";
            yield return new WaitForSeconds(1f);
            saveText.text += ".";
            yield return new WaitForSeconds(1f);
            saveGO.SetActive(false);
            saveText.text = "SAVING";
            openSaveMessage = false;
    }

    IEnumerator OpenLoadMessage()
    {
        openLoadMessage = true;
        loadGO.SetActive(true);
        yield return new WaitForSeconds(1f);
        loadText.text += ".";
        yield return new WaitForSeconds(1f);
        loadText.text += ".";
        yield return new WaitForSeconds(1f);
        loadText.text += ".";
        yield return new WaitForSeconds(1f);
        loadGO.SetActive(false);
        loadText.text = "LOADING";
        openLoadMessage = false;
    }
}
[System.Serializable]
public class SaveToJsonData
{
    public List<ItemInInventory> content;
    public float currentLevel;
    public int currentGold;
    public int currentGems;
    public float currentHealth;
    public float currentHunger;
    public float currentThirst;
}