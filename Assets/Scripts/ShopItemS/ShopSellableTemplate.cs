using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopSellableTemplate : MonoBehaviour
{
    public TMP_Text titleTxt;
    public Image iconImg;
    public TMP_Text descriptionTxt;
    public TMP_Text sellGoldTxt;
    public TMP_Text sellGemsTxt;
    public Image gemsIconImg;
}
