using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Interact : MonoBehaviour
{
    [SerializeField]
    private float interactRange = 2.6f;

    public InteractBehaviour playerInteractBehaviour;

    public CraftingSystem craftingSystem;

    public ShopManager shopManager;

    [Header("UI Text")]
    [SerializeField]
    private GameObject interactText;

    [SerializeField]
    private GameObject onPickupGO;

    [SerializeField]
    private GameObject onCoinsAddGO;

    [SerializeField]
    private LayerMask layerMask;

    private Animator onPickupAnim;
    private Text onPickupText;
    private Animator onCoinsAddAnim;
    private Text onCoinsAddText;

    private void Start()
    {
        onPickupAnim = onPickupGO.GetComponent<Animator>();
        onPickupText = onPickupGO.GetComponentInChildren<Text>();
        onCoinsAddAnim = onCoinsAddGO.GetComponent<Animator>();
        onCoinsAddText = onCoinsAddGO.GetComponentInChildren<Text>();
    }

    void Update()
    {
        RaycastHit hit;

        if(Physics.Raycast(transform.position, transform.forward, out hit, interactRange, layerMask))
        {
            interactText.SetActive(true);

            if (hit.transform.CompareTag("Item"))
            {
                interactText.GetComponent<Text>().text = "Press F to pickup";

                if (Input.GetKeyDown(KeyCode.F))
                {
                    playerInteractBehaviour.DoPickup(hit.transform.gameObject.GetComponent<Item>());
                    StartCoroutine(PickupItemPanel(hit.transform.gameObject.GetComponent<Item>()));
                } 
            }

            if (hit.transform.CompareTag("Harvestable"))
            {
                interactText.GetComponent<Text>().text = "Press F to harvest";
                if (Input.GetKeyDown(KeyCode.F))
                {
                    playerInteractBehaviour.DoHarvest(hit.transform.gameObject.GetComponent<Harvestable>());
                } 
            }

            if (hit.transform.CompareTag("Interactable"))
            {
                    Chest chestDetected = hit.transform.GetComponent<Chest>();
                    SecretBox secretBoxDetected = hit.transform.GetComponent<SecretBox>();
                    Door doorDetected = hit.transform.GetComponent<Door>();
                    ShopHolder shopHolderDetected = hit.transform.GetComponent<ShopHolder>();
                    ButtonAction buttonActionDetected = hit.transform.GetComponent<ButtonAction>();

                if (chestDetected != null)
                {
                    interactText.GetComponent<Text>().text = "Press F to open";
                    if (Input.GetKeyDown(KeyCode.F))
                    {
                        playerInteractBehaviour.DoOpenChest(chestDetected);
                    }
                }
                   
                if(secretBoxDetected != null)
                {
                    interactText.GetComponent<Text>().text = "Press F to open";
                    if (Input.GetKeyDown(KeyCode.F))
                    {
                        StartCoroutine(PickupCoinsPanel(hit.transform.gameObject.GetComponent<SecretBox>()));
                        playerInteractBehaviour.DoOpenSecretBox(secretBoxDetected);
                    }
                }

                if(doorDetected != null)
                {
                    interactText.GetComponent<Text>().text = "Press F to open";
                    if (Input.GetKeyDown(KeyCode.F))
                    {
                        playerInteractBehaviour.DoOpenDoor(doorDetected);
                    } 
                }

                if(shopHolderDetected != null)
                {
                    interactText.GetComponent<Text>().text = "Press F to open shop";
                    if (Input.GetKeyDown(KeyCode.F))
                    {
                        shopManager.OpenShopUI(shopHolderDetected.itemsBuyableInShop, shopHolderDetected.itemsSellableInShop);
                    } 
                }

                if(buttonActionDetected != null)
                {
                    interactText.GetComponent<Text>().text = "Press F to interact";
                    if (Input.GetKeyDown(KeyCode.F))
                    {
                        buttonActionDetected.OpenDungeonDoor();
                    }
                }
            }
        }
        else
        {
            interactText.SetActive(false);
        }
    }

    IEnumerator PickupItemPanel(Item item)
    {
        onPickupGO.SetActive(true);
        onPickupAnim.SetTrigger("PickupOpenPanel");
        onPickupText.text = "You picked up " + item.name;
        yield return new WaitForSeconds(1f);
        onPickupAnim.SetTrigger("PickupClosePanel");
        yield return new WaitForSeconds(1f);
        onPickupGO.SetActive(false);
    }

    IEnumerator PickupCoinsPanel(SecretBox secretBox)
    {
        onCoinsAddGO.SetActive(true);
        onCoinsAddAnim.SetTrigger("CoinsAddOpenPanel");
        onCoinsAddText.text = "You earned " + secretBox.goldToEarn.ToString();
        yield return new WaitForSeconds(4f);
        onCoinsAddGO.SetActive(false);
    }
}
