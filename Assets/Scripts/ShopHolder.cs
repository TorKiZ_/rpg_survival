using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopHolder : MonoBehaviour
{
    public ItemData[] itemsBuyableInShop;
    public ItemData[] itemsSellableInShop;
}
