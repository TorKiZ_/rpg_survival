using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestItemActions : MonoBehaviour
{
    [Header("OTHER SCRIPTS REFERENCES")]

    [SerializeField]
    private PlayerStats playerStats;

    [Header("CHEST SYSTEM VARIABLES")]

    public GameObject chestActionPanel;

    [Header("ACTIONS BUTTONS")]
    [SerializeField] 
    private GameObject useItemButton;

    [SerializeField]
    private GameObject dropItemButton;

    [SerializeField]
    private GameObject deleteItemButton;

    [SerializeField]
    private GameObject moveItemButton;

    [HideInInspector]
    public ItemData itemCurrentlySelected;
    public void OpenChestActionPanel(ItemData item, Vector3 slotPosition)
    {
        itemCurrentlySelected = item;

        if (item == null)
        {
            chestActionPanel.SetActive(false);
            return;
        }

        switch (item.itemType)
        {
            case ItemType.Ressource:
                useItemButton.SetActive(false);
                break;
            case ItemType.Equipment:
                useItemButton.SetActive(false);
                break;
            case ItemType.Consummable:
                useItemButton.SetActive(true);
                break;
        }

        chestActionPanel.transform.position = slotPosition;
        chestActionPanel.SetActive(true);
    }

    public void CloseChestActionPanel()
    {
        chestActionPanel.SetActive(false);
        itemCurrentlySelected = null;
    }

    public void UseChestActionButton()
    {
        playerStats.ConsumeItem(itemCurrentlySelected.healthEffect, itemCurrentlySelected.hungerEffect, itemCurrentlySelected.thirstEffect, itemCurrentlySelected.expGain);
        ChestSystem.instance.RemoveChestItem(itemCurrentlySelected);
        CloseChestActionPanel();
    }

    public void DropChestActionButton()
    {
        GameObject instantiatedItem = Instantiate(itemCurrentlySelected.prefab);
        instantiatedItem.transform.position = ChestSystem.instance.currentChest.dropPoint.position;
        ChestSystem.instance.RemoveChestItem(itemCurrentlySelected);
        ChestSystem.instance.RefreshChestContent();
        CloseChestActionPanel();
    }
    public void DeleteChestActionButton()
    {
        ChestSystem.instance.RemoveChestItem(itemCurrentlySelected);
        ChestSystem.instance.RefreshChestContent();
        CloseChestActionPanel();
    }

    public void MoveChestActionButton()
    {
        Inventory.instance.AddItem(itemCurrentlySelected);
        ChestSystem.instance.RemoveChestItem(itemCurrentlySelected);
        CloseChestActionPanel();
    }
}
