using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour
{
    [Header("Other elements references")]
    [SerializeField]
    private Animator animator;

    [SerializeField]
    private MoveBehaviour playerMovementScript;

    [SerializeField]
    private AudioSource dieSound;

    [Header("Health")]
    [SerializeField]
    private float maxHealth = 100f;
    public float currentHealth;

    [SerializeField]
    private Image healthBarFill;

    [SerializeField]
    private float healthDecreaseRateForHungerAndThirst;

    [Header("Hunger")]
    [SerializeField]
    private float maxHunger = 100f;
    public float currentHunger;

    [SerializeField]
    private Image hungerBarFill;

    [SerializeField]
    private float hungerDecreaseRate;

    [Header("Thirst")]
    [SerializeField]
    private float maxThirst = 100f;
    public float currentThirst;

    [SerializeField]
    private Image thirstBarFill;

    [SerializeField]
    private float thirstDecreaseRate;

    [Header("Experience")]

    [SerializeField]
    public float maxExperience;
    public float currentExperience = 0;

    [SerializeField]
    public float playerLevel = 1;

    [SerializeField]
    private Image experienceBarFill;

    [HideInInspector]
    public float currentArmorPoints;

    [Header("Audio Settings")]
    [SerializeField]
    private AudioSource getHitSound;

    [HideInInspector]
    public bool isDead = false;

    [HideInInspector]
    public bool canReduceHungerThirst = true;

    public static PlayerStats instance;

    void Awake()
    {
        instance = this;
        currentHealth = maxHealth;
        currentHunger = maxHunger;
        currentThirst = maxThirst;
    }

    // Update is called once per frame
    void Update()
    {
       UpdateHungerAndThirstBarFill();
    }

    public void TakeDamage(float damage, bool overTime = false)
    {
        if(overTime)
        {
            currentHealth -= damage * Time.deltaTime;
        }
        else
        {
            currentHealth -= (damage * (1 - (currentArmorPoints / 100)));
            getHitSound.Play();
        }

        if(currentHealth <= 0 && !isDead)
        {
            Die();
        }
 
        UpdateHealthBarFill();
    }

    private void Die()
    {
        isDead= true;
        playerMovementScript.canMove = false;

        hungerDecreaseRate = 0;
        thirstDecreaseRate = 0;

        animator.SetTrigger("Die");
    }

    public void ConsumeItem(float health, float hunger, float thirst, float exp)
    {
        currentHealth += health;

        if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        currentHunger += hunger;

        if(currentHunger > maxHunger)
        {
            currentHunger = maxHunger;
        }

        currentThirst += thirst;

        if (currentThirst > maxThirst)
        {
            currentThirst = maxThirst;
        }

        currentExperience += exp;

        if(currentExperience >= maxExperience)
        {
            GainLevel(exp);
        }

        UpdateHealthBarFill();
        UpdateHungerAndThirstBarFill();
        UpdateExpBarFill();
    }

    void GainLevel(float expAdded)
    {
        float rest = maxExperience - currentExperience;
        float result = expAdded - rest;
        currentExperience = result;
        //Play a sound
        //Open a uiPanel
        playerLevel += 1;
    }

    public void UpdateExpBarFill()
    {
        experienceBarFill.fillAmount = currentExperience / maxExperience;
    }

    void UpdateHealthBarFill()
    {
        healthBarFill.fillAmount = currentHealth / maxHealth;
    }
    
    void UpdateHungerAndThirstBarFill()
    {
        if(canReduceHungerThirst)
        {
            currentHunger -= hungerDecreaseRate * Time.deltaTime;
            currentThirst -= thirstDecreaseRate * Time.deltaTime;
        }

        //Keep the positiv
        currentHunger = currentHunger < 0 ? 0 : currentHunger;
        currentThirst = currentThirst < 0 ? 0 : currentThirst;

        //Visual
        hungerBarFill.fillAmount = currentHunger / maxHunger;
        thirstBarFill.fillAmount = currentThirst / maxThirst;

        //x2 if both hunger/thirst = 0
        if(currentHunger <= 0 || currentThirst <= 0)
        {
            TakeDamage((currentHunger <= 0 && currentThirst <= 0 ? healthDecreaseRateForHungerAndThirst * 2 : healthDecreaseRateForHungerAndThirst), true);
        }
    }

    public void PlayDieSound()
    {
        dieSound.Play();
    }
}
