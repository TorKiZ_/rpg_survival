using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNNightCycle : MonoBehaviour
{
    [SerializeField]
    private Material skybox;

    [SerializeField]
    private Transform sunTransform;

    private float elapsedTime = 0f;
    private float oneDayElapsedTime = 100f;
    private float timeScale = 0.5f;
    private static readonly int Rotation = Shader.PropertyToID("_Rotation");
    private static readonly int Exposure = Shader.PropertyToID("_Exposure");

    private void Start()
    {
        SetNight();
    }

    void Update()
    {
            elapsedTime += Time.deltaTime;
            skybox.SetFloat(Rotation, elapsedTime * timeScale);

        if(transform.rotation.x >= 180)
        {
            SetNight();
        }
        else
        {
            SetDay();
        }
    }

    public void SetDay()
    {
        skybox.SetFloat(Exposure, 1f);
    }

    public void SetNight()
    {
        skybox.SetFloat(Exposure, 0.15f);
    }
}
