using UnityEngine;

public class ItemActions : MonoBehaviour
{
    [Header("OTHER SCRIPTS REFERENCES")]

    [SerializeField]
    private Equipment equipment;

    [SerializeField]
    private PlayerStats playerStats;

    [SerializeField]
    private CraftingSystem craftingSystem;

    [Header("INVENTORY SYSTEM VARIABLES")]

    public GameObject actionPanel;

    [SerializeField]
    public Transform dropPoint;

    [Header("ACTIONS BUTTONS")]
    [SerializeField]
    private GameObject useItemButton;

    [SerializeField]
    private GameObject useRecipeBtn;

    [SerializeField]
    private GameObject equipItemButton;

    [SerializeField]
    private GameObject dropItemButton;

    [SerializeField]
    private GameObject deleteItemButton;

    [SerializeField]
    private GameObject moveItemButton;

    [HideInInspector]
    public ItemData itemCurrentlySelected;

    [Header("SOUNDS EFFECT")]
    [SerializeField]
    private AudioSource useItemActionClip;

    [SerializeField]
    private AudioSource equipItemActionClip;

    public void OpenActionPanel(ItemData item, Vector3 slotPosition)
    {
        itemCurrentlySelected = item;
        moveItemButton.SetActive(false);

        if (item == null)
        {
            actionPanel.SetActive(false);
            return;
        }

            switch (item.itemType)
            {
                case ItemType.Ressource:
                    useItemButton.SetActive(false);
                    equipItemButton.SetActive(false);
                    useRecipeBtn.SetActive(false);
                    break;
                case ItemType.Equipment:
                    useItemButton.SetActive(false);
                    useRecipeBtn.SetActive(false);
                    equipItemButton.SetActive(true);
                    break;
                case ItemType.Consummable:
                    useItemButton.SetActive(true);
                    equipItemButton.SetActive(false);
                    useRecipeBtn.SetActive(false);
                    break;
                case ItemType.Recipe:
                    useItemButton.SetActive(false);
                    equipItemButton.SetActive(false);
                    useRecipeBtn.SetActive(true);
                break;
        }

        if (Inventory.instance.isInventoryOpen == ChestSystem.instance.isChestOpen)
        {
            moveItemButton.SetActive(true);
        }

        actionPanel.transform.position = slotPosition;
        actionPanel.SetActive(true);
    }

    public void CloseActionPanel()
    {
        actionPanel.SetActive(false);
        itemCurrentlySelected = null;
    }

 /*   public void LearnRecipBtn()
    {
        craftingSystem.AddRecipe(itemCurrentlySelected);
        Inventory.instance.RemoveItem(itemCurrentlySelected);
        CloseActionPanel();
    }
 */
    public void UseActionButton()
    {
        useItemActionClip.Play();
        playerStats.ConsumeItem(itemCurrentlySelected.healthEffect, itemCurrentlySelected.hungerEffect, itemCurrentlySelected.thirstEffect, itemCurrentlySelected.expGain);
        Inventory.instance.RemoveItem(itemCurrentlySelected);
        CloseActionPanel();
    }

    public void EquipAction()
    {
        equipItemActionClip.Play();
        equipment.EquipActionButton();
        CloseActionPanel();
    }

    public void DropActionButton()
    {
        GameObject instantiatedItem = Instantiate(itemCurrentlySelected.prefab);
        instantiatedItem.transform.position = dropPoint.position;
        Inventory.instance.RemoveItem(itemCurrentlySelected);
        Inventory.instance.RefreshContent();
        CloseActionPanel();
    }
    public void DeleteActionButton()
    {
        Inventory.instance.RemoveItem(itemCurrentlySelected);
        Inventory.instance.RefreshContent();
        CloseActionPanel();
    }

    public void MoveActionButton()
    {
        ChestSystem.instance.AddItem(itemCurrentlySelected);
        Inventory.instance.RemoveItem(itemCurrentlySelected);
        CloseActionPanel();
    }
}
