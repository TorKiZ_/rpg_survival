using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChestSystem : MonoBehaviour
{
    [SerializeField]
    private Transform chestSlotParent;

    [SerializeField]
    private InteractBehaviour playerInteractBehaviour;

    public Sprite emptySlotVisual;

    [HideInInspector]
    public bool isChestOpen = false;

    [SerializeField]
    public Chest currentChest;

    [SerializeField]
    private GameObject chestPanel;

    [SerializeField]
    private ChestItemActions chestItemActionPanel;

    public static ChestSystem instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        CloseChest();
    }
    public void AddItem(ItemData item)
    {
        ChestItem[] itemInChest = currentChest.chestItems.Where(elem => elem.item == item).ToArray();

        bool itemAdded = false;

        if (itemInChest.Length > 0 && item.stackable)
        {
            for (int i = 0; i < itemInChest.Length; i++)
            {
                if (itemInChest[i].count < item.maxStack)
                {
                    itemAdded = true;
                    itemInChest[i].count++;
                    break;
                }
            }

            if (!itemAdded)
            {
                currentChest.chestItems.Add(new ChestItem { item = item, count = 1 });
            }
        }
        else
        {
            currentChest.chestItems.Add(new ChestItem { item = item, count = 1 });
        }

        RefreshChestContent();
    }
    public void RemoveChestItem(ItemData item)
    {
        ChestItem itemInChest = currentChest.chestItems.Where(elem => elem.item == item).FirstOrDefault();

        if (itemInChest != null && itemInChest.count > 1)
        {
            itemInChest.count--;
        }
        else
        {
            currentChest.chestItems.Remove(itemInChest);
        }
        RefreshChestContent();
    }

    public List<ChestItem> GetChestContent()
    {
        return currentChest.chestItems;
    }

    public void OpenChest()
    {
        if (!isChestOpen)
        {
            RefreshChestContent();
            chestPanel.SetActive(true);
            isChestOpen = true;
        }
        else
        {
            CloseChest();
        }
    }

    public void CloseChest()
    {
        StartCoroutine(CloseChestAnimated());
    }

    public void RefreshChestContent()
    {
        //On vide tout les slots/visuels
        for (int i = 0; i < chestSlotParent.childCount; i++)
        {
            Slot currentSlot = chestSlotParent.GetChild(i).GetComponent<Slot>();

            currentSlot.itemData = null;
            currentSlot.itemVisual.sprite = emptySlotVisual;
            currentSlot.countText.enabled = false;
        }

        //On peuple les slots selon le contenu r�el de l'inventaire
        for (int i = 0; i < currentChest.chestItems.Count; i++)
        {
            Slot currentSlot = chestSlotParent.GetChild(i).GetComponent<Slot>();

            currentSlot.itemData = currentChest.chestItems[i].item;
            currentSlot.itemVisual.sprite = currentChest.chestItems[i].item.visual;

            if (currentSlot.itemData.stackable)
            {
                currentSlot.countText.enabled = true;
                currentSlot.countText.text = currentChest.chestItems[i].count.ToString();
            }
            
        }
    }

    IEnumerator CloseChestAnimated()
    {
        while (isChestOpen)
        {
            chestPanel.SetActive(false);
            chestItemActionPanel.chestActionPanel.SetActive(false);
            ToolTipSystem.instance.Hide();
            playerInteractBehaviour.ReEnablePlayerMovement();
            yield return new WaitForSeconds(1);
            currentChest.audioSources.closeChest.Play();
            currentChest.animator.SetTrigger("isClosed");
            yield return new WaitForSeconds(1);
            currentChest = null;
            isChestOpen = false;
        }
    }
}
