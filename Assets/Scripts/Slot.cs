using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public ItemData itemData;
    public Image itemVisual;
    public Text countText;
    public bool chestSlot;


    [SerializeField]
    private ItemActions itemActions;

    [SerializeField]
    private ChestItemActions chestItemActions;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(itemData != null)
        {
            ToolTipSystem.instance.Show(itemData.description, itemData.name);
        }
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        ToolTipSystem.instance.Hide();
    }

    public void ClickOnSlot()
    {
        if(chestSlot)
        {
            chestItemActions.OpenChestActionPanel(itemData, transform.position);
        }
        else
        {
            itemActions.OpenActionPanel(itemData, transform.position);
        }
    }
}
