using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Chest : MonoBehaviour
{
    [SerializeField]
    public Animator animator;

    [SerializeField]
    public List<ChestItem> chestItems = new List<ChestItem>();

    [SerializeField]
    public Transform dropPoint;

    [SerializeField]
    public AudioSources audioSources;

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && ChestSystem.instance.isChestOpen)
        {
            ChestSystem.instance.CloseChest();
        }
    }
}

[System.Serializable]
public class ChestItem
{
    public ItemData item;
    public int count;
}

[System.Serializable]
public class AudioSources
{
    public AudioSource openChest;
    public AudioSource closeChest;
}