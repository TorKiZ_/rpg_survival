using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine;

public class FireEffect : MonoBehaviour
{
    public float fireDamageDealt = 0;
    public bool fireEffect = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine(DoFireDamage());
        }
    }
    IEnumerator DoFireDamage()
    {
        fireEffect= true;
        while(fireEffect)
        {
            float baseDamage = fireDamageDealt;
            fireDamageDealt = fireDamageDealt / 5f;
            PlayerStats.instance.TakeDamage(fireDamageDealt);
            yield return new WaitForSeconds(0.5f);
            PlayerStats.instance.TakeDamage(fireDamageDealt);
            yield return new WaitForSeconds(0.5f);
            PlayerStats.instance.TakeDamage(fireDamageDealt);
            yield return new WaitForSeconds(0.5f);
            PlayerStats.instance.TakeDamage(fireDamageDealt);
            yield return new WaitForSeconds(0.5f);
            PlayerStats.instance.TakeDamage(fireDamageDealt);
            yield return new WaitForSeconds(0.5f);
            fireDamageDealt = baseDamage;
            fireEffect= false;
        }
    }
}
