using System.Collections;
using UnityEngine;

public class AttackBehaviour : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private Animator animator;

    [SerializeField]
    private Equipment equipmentSystem;

    [SerializeField]
    private UIManager uiManager;

    [SerializeField]
    private InteractBehaviour interactBehaviour;

    [SerializeField]
    private PlayerStats playerStats;

    [Header("FlashLight")]
    [SerializeField]
    private Light flashLight;

    [SerializeField]
    private AudioSource flashLightSound;

    private bool isActive = true;

    [Header("Configuration")]

    private bool isAttacking;

    [SerializeField]
    private float attackRange;

    [SerializeField]
    private LayerMask layerMask;

    [SerializeField]
    private Vector3 attackOffset;

    private AudioSource currentAudioSource;

    void Update()
    {
         Debug.DrawRay(transform.position + attackOffset, transform.forward * attackRange, Color.red);
        if (Input.GetMouseButtonDown(0))
        {
            if (CanAttack())
            {
                isAttacking = true;
                SendAttack();
                animator.SetTrigger("Attack");
            }
            
            if(equipmentSystem.equipedWeaponItem != null && equipmentSystem.equipedWeaponItem.attackPoints == 0)
            {
                    if (!isActive)
                    {
                        StartCoroutine(FlashLightSystemOpen());
                    }
                    else
                    {
                        StartCoroutine(FlashLightSystemClose());
                    }
            }
        }
    }

    IEnumerator FlashLightSystemOpen()
    {
            flashLightSound.Play();
            yield return new WaitForSeconds(0.7f);
            flashLight.enabled = true;
            isActive = true;
    }

    IEnumerator FlashLightSystemClose()
    {
        flashLightSound.Play();
        yield return new WaitForSeconds(0.7f);
        flashLight.enabled = false;
        isActive = false;
    }

    void SendAttack()
    {
        RaycastHit hit;

        if(Physics.Raycast(transform.position + attackOffset, transform.forward, out hit, attackRange, layerMask))
        {
            if (hit.transform.CompareTag("AI"))
            {
                EnemyAI enemy = hit.transform.GetComponent<EnemyAI>();
                enemy.TakeDamage(equipmentSystem.equipedWeaponItem.attackPoints);
            }
            if (hit.transform.CompareTag("Destroyable"))
            {
                DestructedItem currentDestroyable = hit.transform.GetComponent<DestructedItem>();
                currentDestroyable.DestroyItem();
            }

        }
        PlayAttackSound();
    }

    bool CanAttack()
    {
        return equipmentSystem.equipedWeaponItem != null && !isAttacking && !uiManager.atLeastOnePanelOpened && !interactBehaviour.isBusy && !playerStats.isDead && equipmentSystem.equipedWeaponItem.attackPoints != 0;
    }

    public void AttackFinished()
    {
        isAttacking = false;
    }

    void PlayAttackSound()
    {
        equipmentSystem.equipedWeaponAudio.Play();
    }
}
