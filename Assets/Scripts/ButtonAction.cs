using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAction : MonoBehaviour
{
    public Animator openDungeonDoor;

    public void OpenDungeonDoor()
    {
        openDungeonDoor.SetTrigger("OpenDoor");
    }
}
