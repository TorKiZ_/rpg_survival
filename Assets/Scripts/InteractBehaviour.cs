using System.Collections;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public class InteractBehaviour : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private MoveBehaviour playerMoveBehaviour;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private Inventory inventory;

    [SerializeField]
    private Equipment equipmentSystem;

    [SerializeField]
    private EquipmentLibrary equipmentLibrary;

    [HideInInspector]
    public bool isBusy = false;

    [Header("AudioClips")]
    [SerializeField]
    private AudioSource pickupSound;

    [Header("Tools Visual")]
    [SerializeField]
    private GameObject pickaxeVisual;

    [SerializeField]
    private GameObject axeVisual;

    private Item currentItem;
    private Chest currentChest;
    private Harvestable currentHarvestable;
    private Tools currentTool;

    private Vector3 spawnItemOffset = new Vector3(0, 0.5f, 0);

    public void DoPickup(Item item)
    {
        if(isBusy) return;
        
        isBusy= true;

        if(inventory.IsFull())
        {
            Debug.Log("Inventory full, can't pick up : " + item.name);
            return;
        }
        currentItem = item;

        animator.SetTrigger("Pickup");
        playerMoveBehaviour.canMove= false;
    }

    public void DoOpenChest(Chest chest)
    {
        if (isBusy) { return; }

        isBusy = true;

        animator.SetTrigger("OpenChest");
        chest.audioSources.openChest.Play();
        ChestSystem.instance.currentChest = chest;
        ChestSystem.instance.currentChest.animator.SetTrigger("isOpen");
        playerMoveBehaviour.canMove= false;
    }

    public void DoHarvest(Harvestable harvestable)
    {
        if (isBusy) return;

        isBusy = true;

        currentTool = harvestable.tools;
        EnableToolGameObjectFromEnum(currentTool);
        currentHarvestable = harvestable;
        animator.SetTrigger("Harvest");
        playerMoveBehaviour.canMove= false;
    }

    public void DoOpenSecretBox(SecretBox secretBox)
    {
        if(isBusy) { return; }

        isBusy= true;

        animator.SetTrigger("OpenSecretBox");
        secretBox.onInteract.Play();
        Inventory.instance.AddCoins(secretBox.goldToEarn);
        secretBox.goldToEarn = 0;
        secretBox.parent.tag = "Untagged";
        secretBox.parent.layer = 0;
    }

    public void DoOpenDoor(Door door)
    {
        if (isBusy) { return; }

        isBusy = true;

        animator.SetTrigger("OpenDoor");

        door.openDoor.Play();

        if (!door.isOpened)
        {
            door.OpenDoor();
        }
        else
        {
            door.CloseDoor();
        }
    }

    IEnumerator BreakHarvestable()
    {
        Harvestable currentlyHarvesting = currentHarvestable;

        currentlyHarvesting.gameObject.layer = LayerMask.NameToLayer("Default");

        if(currentlyHarvesting.disableKinematicOnHarvest)
        {
            Rigidbody rigidbody = currentlyHarvesting.gameObject.GetComponent<Rigidbody>();
            rigidbody.isKinematic = false;
            rigidbody.AddForce(transform.forward * 800, ForceMode.Impulse);
        }

        yield return new WaitForSeconds(currentlyHarvesting.destroyDelay);

        for (int i = 0; i < currentlyHarvesting.harvestableItems.Length; i++)
        {
            Ressource ressource = currentlyHarvesting.harvestableItems[i];

            if(Random.Range(1, 101) <= ressource.dropChance)
            {
                GameObject instantiatedRessource = Instantiate(ressource.itemData.prefab);
                instantiatedRessource.transform.position = currentlyHarvesting.transform.position + spawnItemOffset;
            }
        }
        Destroy(currentlyHarvesting.gameObject);
    }

    public void AddItemToInventory()
    {
        if(currentItem != null)
        {
            inventory.AddItem(currentItem.itemData);
            Destroy(currentItem.gameObject);
        }
    }

    public void OpenChestPanel()
    {
        ChestSystem.instance.OpenChest();
    }

    public void PlayPickupSound()
    {
        pickupSound.Play();
    }

    public void ReEnablePlayerMovement()
    {
        EnableToolGameObjectFromEnum(currentTool, false);
        playerMoveBehaviour.canMove = true;
        isBusy = false;
    }

    public void EnableToolGameObjectFromEnum(Tools toolType, bool enabled = true)
    {
        EquipmentLibraryItem equipmentLibraryItem = equipmentLibrary.content.Where(elem => elem.itemData == equipmentSystem.equipedWeaponItem).FirstOrDefault();

        if (equipmentLibraryItem != null)
        {
            for (int i = 0; i < equipmentLibraryItem.elementToDisable.Length; i++)
            {
                equipmentLibraryItem.elementToDisable[i].SetActive(enabled);
            }

            equipmentLibraryItem.itemPrefab.SetActive(!enabled);
        }

        switch (toolType)
        {
            case Tools.Pickaxe:
                pickaxeVisual.SetActive(enabled);
                break;
            case Tools.Axe:
                axeVisual.SetActive(enabled);
                break;
        }
    }
}
