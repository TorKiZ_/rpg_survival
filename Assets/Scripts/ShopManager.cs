using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    [Header("")]
    [SerializeField]
    private CraftingSystem craftingSystem;

    [Header("Properties")]
    public GameObject shopUI;
    public int amountGold;
    public int amountGems;
    public TMP_Text coinsUI;
    public TMP_Text gemsUI;

    [Header("Buyable")]
    public ItemData[] shopItemBuyableSO = null;
    public GameObject[] shopBuyablePanelsGO;
    public ShopBuyableTemplate[] shopBuyablePanels;
    public Button[] purchaseBuyableGoldBtn;
    public Button[] purchaseBuyableGemBtn;
    public GameObject buyPanelParent;

    [Header("Sellable")]
    public ItemData[] shopItemSellableSO = null;
    public GameObject[] shopSellablePanelsGO;
    public ShopSellableTemplate[] shopSellablePanels;
    public Button[] purchaseSellableGoldBtn;
    public Button[] purchaseSellableGemBtn;
    public GameObject sellPanelParent;
    public int priceDivision = 1;

    public bool isUIOpen = false;
    private int panelSelector = 2;

    public void OpenShopUI(ItemData[] itemsBuyableInCurrentShop, ItemData[] itemsSellableInCurrentShop)
    {
        craftingSystem.craftingPanel.SetActive(false);
        Inventory.instance.CloseInventory();
        shopItemBuyableSO = itemsBuyableInCurrentShop;
        shopItemSellableSO = itemsSellableInCurrentShop;
        shopUI.SetActive(true);
          Inventory.instance.openItemActionClip.Play();
        for (int i = 0; i < shopItemSellableSO.Length; i++)
        {
            shopSellablePanelsGO[i].SetActive(true);
        }
        for (int i = 0; i < shopItemBuyableSO.Length; i++)
        {
            shopBuyablePanelsGO[i].SetActive(true);
        }
        buyPanelParent.SetActive(true);
        sellPanelParent.SetActive(false);
        coinsUI.text = "You have " + Inventory.instance.currentGold.ToString();
          gemsUI.text = Inventory.instance.currentGems.ToString();
          LoadPanels();
          CheckPurchaseable();
          CheckSellable();
        isUIOpen = true;
    }

    public void CloseShopUI()
    {
        Inventory.instance.closeItemActionClip.Play();

        for (int i = 0; i < shopItemSellableSO.Length; i++)
        {
            shopSellablePanelsGO[i].SetActive(false);
        }

        for (int i = 0; i < shopItemBuyableSO.Length; i++)
        {
           shopBuyablePanelsGO[i].SetActive(false);
        }

        shopUI.SetActive(false);
        shopItemBuyableSO = null;
        shopItemSellableSO= null;
        isUIOpen = false;
    }

    public void AddCoins()
    {
        Inventory.instance.AddCoins(amountGold);
        coinsUI.text = "You have " + Inventory.instance.currentGold.ToString();
        CheckPurchaseable();
    }

    public void AddGems()
    {
        Inventory.instance.AddGems(amountGems);
        gemsUI.text = Inventory.instance.currentGems.ToString();
        CheckPurchaseable();
    }

    public void CheckPurchaseable()
    {
        for (int i = 0; i < shopItemBuyableSO.Length; i++)
        {
            if (Inventory.instance.currentGold >= shopItemBuyableSO[i].baseGoldCost && shopItemBuyableSO[i].baseGoldCost > 0)
                purchaseBuyableGoldBtn[i].interactable = true;
            else
                purchaseBuyableGoldBtn[i].interactable = false;

            if (Inventory.instance.currentGems >= shopItemBuyableSO[i].baseGemCost && shopItemBuyableSO[i].baseGemCost > 0)
                purchaseBuyableGemBtn[i].interactable = true;
            else
                purchaseBuyableGemBtn[i].interactable = false;
        }
    }

    public void CheckSellable()
    {

        for (int i = 0; i < shopItemSellableSO.Length; i++)
        {
            if (CheckIfItemIsInInventory(shopItemSellableSO[i]))
            {
                purchaseSellableGoldBtn[i].interactable = true;

                if(shopItemSellableSO[i].baseGemCost != 0)
                {
                    purchaseSellableGemBtn[i].interactable = true;
                }
                else
                {
                    purchaseSellableGemBtn[i].interactable = false;
                }
            }
            else
            {
                purchaseSellableGoldBtn[i].interactable = false;
                purchaseSellableGemBtn[i].interactable= false;
            }
        }
    }

    public bool CheckIfItemIsInInventory(ItemData currentShopSell)
    {
        List<ItemInInventory> contentOfInventory = Inventory.instance.GetContent();
        List<ItemData> itemDatas = new List<ItemData>();

        for (int i = 0; i < contentOfInventory.Count; i++)
        {
            itemDatas.Add(contentOfInventory[i].itemData);
            Debug.Log("Adding" + contentOfInventory[i].itemData.name);
        }

        return itemDatas.Contains(currentShopSell);
    }

    public void PurchaseItemWGold(int btnNo)
    {
        if(Inventory.instance.currentGold >= shopItemBuyableSO[btnNo].baseGoldCost)
        {
            Inventory.instance.currentGold = Inventory.instance.currentGold - shopItemBuyableSO[btnNo].baseGoldCost;
            coinsUI.text = "You have " + Inventory.instance.currentGold.ToString();
            Inventory.instance.AddItem(shopItemBuyableSO[btnNo]);
            CheckPurchaseable();
            CheckSellable();
        }
    }

    public void PurchaseItemWGems(int btnNo)
    {
        if (Inventory.instance.currentGems >= shopItemBuyableSO[btnNo].baseGemCost)
        {
            Inventory.instance.currentGems = Inventory.instance.currentGems - shopItemBuyableSO[btnNo].baseGemCost;
            gemsUI.text = Inventory.instance.currentGems.ToString();
            Inventory.instance.AddItem(shopItemBuyableSO[btnNo]);
            CheckPurchaseable();
            CheckSellable();
        }
    }

    public void SellItemWGold(int btnNo)
    {
            Inventory.instance.RemoveItem(shopItemSellableSO[btnNo]);
            Inventory.instance.AddCoins(shopItemSellableSO[btnNo].baseGoldSellPrice);
            coinsUI.text = "You have " + Inventory.instance.currentGold.ToString();
            CheckPurchaseable();
            CheckSellable();
    }

    public void SellItemWGems(int btnNo)
    {
            Inventory.instance.RemoveItem(shopItemSellableSO[btnNo]);
            Inventory.instance.AddGems(shopItemSellableSO[btnNo].baseGemSellPrice);
            gemsUI.text = Inventory.instance.currentGems.ToString();
            CheckPurchaseable();
            CheckSellable();
    }

    public void LoadPanels()
    {
        for(int i = 0; i < shopItemBuyableSO.Length; i++)
        {
            shopBuyablePanels[i].titleTxt.text = shopItemBuyableSO[i].name;
            shopBuyablePanels[i].iconImg.sprite = shopItemBuyableSO[i].visual;
            shopBuyablePanels[i].descriptionTxt.text = shopItemBuyableSO[i].description;
            shopBuyablePanels[i].costGoldTxt.text = shopItemBuyableSO[i].baseGoldCost.ToString();
            shopBuyablePanels[i].costGemsTxt.text = shopItemBuyableSO[i].baseGemCost.ToString();

        }

        for (int i = 0; i < shopItemSellableSO.Length; i++)
        {
            shopSellablePanels[i].titleTxt.text = shopItemSellableSO[i].name;
            shopSellablePanels[i].iconImg.sprite = shopItemSellableSO[i].visual;
            shopSellablePanels[i].descriptionTxt.text = shopItemSellableSO[i].description;
            shopSellablePanels[i].sellGoldTxt.text = shopItemSellableSO[i].baseGoldSellPrice.ToString();
            shopSellablePanels[i].sellGemsTxt.text = shopItemSellableSO[i].baseGemSellPrice.ToString();
        }
    }

    public bool CheckOpenBool()
    {
        return isUIOpen;
    }
}
