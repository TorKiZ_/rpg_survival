using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseSystem : MonoBehaviour
{
    [SerializeField]
    private GameObject optionPanel;

    [SerializeField]
    private UIManager uiManager;

    [SerializeField]
    private MoveBehaviour moveBehaviour;

    [SerializeField]
    private PlayerStats playerStats;

    private bool isOpened = false;

    void Start()
    {
        CloseOptionPanel();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && !isOpened)
        {
            OpenOptionsPanel();
        }
        else if(Input.GetKeyDown(KeyCode.Escape) && isOpened)
        {
            CloseOptionPanel();
        }
    }

    public void OpenOptionsPanel()
    {
        for(int i = 0; i < uiManager.UIPanels.Length; i++)
        {
            uiManager.UIPanels[i].gameObject.SetActive(false);
        }
        moveBehaviour.canMove = false;
        playerStats.canReduceHungerThirst = false;
        optionPanel.SetActive(true);
        isOpened = true;
    }

    public void CloseOptionPanel()
    {
        optionPanel.SetActive(false);
        moveBehaviour.canMove = true;
        playerStats.canReduceHungerThirst = true;
        isOpened= false;
    }

    public void ResumeBtn()
    {
        CloseOptionPanel();
    }

    public void SaveBtn()
    {
        SaveData.instance.SaveToJson();
    }

    public void ExitBtn()
    {
        SceneManager.LoadScene("StartingScreen");
    }
}
