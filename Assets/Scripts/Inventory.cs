using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
using System;

public class Inventory : MonoBehaviour
{
    [Header("OTHER SCRIPTS REFERENCES")]

    [SerializeField]
    private Equipment equipment;

    [SerializeField]
    private ItemActions itemActions;

    [SerializeField]
    private CraftingSystem craftingSystem;

    [SerializeField]
    private PlayerStats playerStats;

    [SerializeField]
    private ShopManager shopManager;

    [Header("INVENTORY SYSTEM VARIABLES")]

    [SerializeField]
    public List<ItemInInventory> content = new List<ItemInInventory>();

    [SerializeField]
    private GameObject inventoryPanel;

    [SerializeField]
    private Transform inventorySlotParent;

    public Sprite emptySlotVisual;

    [Header("SOUND EFFECTS")]
    [SerializeField]
    public AudioSource closeItemActionClip;

    [SerializeField]
    public AudioSource openItemActionClip;

    [Header("Other")]
    [HideInInspector]
    public int currentGold;

    [SerializeField]
    private TextMeshProUGUI currentGoldVisual;

    [SerializeField]
    private TextMeshProUGUI currentLevelVisual;

    //[HideInInspector]
    public int currentGems = 0;

    [SerializeField]
    private TextMeshProUGUI currentGemsVisual = null;

    public static Inventory instance;

    const int InventorySize = 24;

    [HideInInspector]
    public bool isInventoryOpen = false;

    private void Awake()
    {
        instance= this;
    }

    private void Start()
    {
        CloseInventory();
        RefreshContent();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if(isInventoryOpen)
            {
                CloseInventory();
                ChestSystem.instance.CloseChest();
            }
            else if (shopManager.isUIOpen)
            {
                shopManager.CloseShopUI();
            }
            else
            {
                OpenInventory();
            }
        }
    }

    public void AddItem(ItemData item)
    {
        ItemInInventory[] itemInInventory = content.Where(elem => elem.itemData == item).ToArray();

        bool itemAdded = false;

        if(itemInInventory.Length > 0 && item.stackable)
        {
            for (int i = 0; i < itemInInventory.Length; i++)
            {
                if (itemInInventory[i].count < item.maxStack)
                {
                    itemAdded = true;
                    itemInInventory[i].count++;
                    break;
                }
            }

            if(!itemAdded)
            {
                content.Add(new ItemInInventory { itemData = item, count = 1 });
            }
        }
        else
        {
            content.Add(new ItemInInventory { itemData = item, count = 1 });
        }

        RefreshContent();
    }

    public void RemoveItem(ItemData item)
    {
        ItemInInventory itemInInventory = content.Where(elem => elem.itemData == item).FirstOrDefault();

        if(itemInInventory != null && itemInInventory.count > 1)
        {
            itemInInventory.count--;
        }
        else
        {
            content.Remove(itemInInventory);
        }

        RefreshContent();
    }

    public List<ItemInInventory> GetContent()
    {
        return content;
    }

    public void OpenInventory()
    {
        OnClickSoundOpen();
        inventoryPanel.SetActive(true);
        isInventoryOpen= true;
        RefreshContent();
    }

    public void CloseInventory()
    {
        if(closeItemActionClip.isActiveAndEnabled)
        {
            OnClickSoundClose();
        }
     
        closeItemActionClip.enabled = true;
        inventoryPanel.SetActive(false);
        itemActions.actionPanel.SetActive(false);
        ToolTipSystem.instance.Hide();
        isInventoryOpen= false;
    }

    public void RefreshContent()
    {
        //On vide tout les slots/visuels
        for (int i = 0; i < inventorySlotParent.childCount; i++)
        {
            Slot currentSlot = inventorySlotParent.GetChild(i).GetComponent<Slot>();

            currentSlot.itemData = null;
            currentSlot.itemVisual.sprite = emptySlotVisual;
            currentSlot.countText.enabled = false;
        }

        //On peuple les slots selon le contenu r�el de l'inventaire
        for (int i = 0; i < content.Count; i++)
        {
            Slot currentSlot = inventorySlotParent.GetChild(i).GetComponent<Slot>();

            currentSlot.itemData = content[i].itemData;
            currentSlot.itemVisual.sprite = content[i].itemData.visual;

            if (currentSlot.itemData.stackable)
            {
                currentSlot.countText.enabled = true;
                currentSlot.countText.text = content[i].count.ToString();
            }
        }
        equipment.UpdateEquipmentsDesequipButtons();
        craftingSystem.UpdateDisplayedRecipes();
        currentGoldVisual.text = currentGold.ToString();

        String levelString = "Lv." + playerStats.playerLevel + " >> " + playerStats.currentExperience.ToString() + " / " + playerStats.maxExperience.ToString();

        currentLevelVisual.text = levelString;
    }

    public bool IsFull()
    {
        return InventorySize == content.Count;
    }

    public void OnClickSoundOpen()
    {
        openItemActionClip.Play();
    }

    public void OnClickSoundClose()
    {
        closeItemActionClip.Play();
    }

    public void AddCoins(int amount)
    {
        currentGold += amount;
    }

    public void AddGems(int amount)
    {
        currentGems += amount;
    }
}

[System.Serializable]
public class ItemInInventory
{
    public ItemData itemData;
    public int count;
}