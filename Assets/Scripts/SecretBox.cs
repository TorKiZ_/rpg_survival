using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecretBox : MonoBehaviour
{
    [Header("Audio")]
    public AudioSource onInteract;

    [Header("Gold to earn")]
    public int goldToEarn;

    public GameObject parent;
}
