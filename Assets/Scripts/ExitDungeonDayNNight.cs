using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitDungeonDayNNight : MonoBehaviour
{
    [SerializeField]
    private GameObject sun;

    private bool isDay = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!isDay)
        {
            sun.SetActive(true);
            sun.GetComponent<Animator>().SetTrigger("DoDayNNight");
            isDay = true;
        }
        else
        {
            sun.SetActive(true);
            sun.GetComponent<Animator>().SetTrigger("StopDayNNight");
            isDay = false;
        }
    }
}
