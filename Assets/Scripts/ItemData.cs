using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

[CreateAssetMenu(fileName = "Item",menuName = "Item/New Item")]
public class ItemData : ScriptableObject
{
    [Header("Data")]
    public string name;
    public string description;
    public Sprite visual;
    public GameObject prefab;
    public bool stackable;
    public int maxStack;

    [Header("ExpGain")]
    public float expGain;

    [Header("Effects")]
    public float healthEffect;
    public float hungerEffect;
    public float thirstEffect;

    [Header("Armor Stats")]
    public float armorPoints;

    [Header("Attack Stats")]
    public float attackPoints;

    [Header("Types")]
    public ItemType itemType;
    public EquipmentType equipmentType;

    [Header("Cost")]
    public int baseGoldCost;
    public int baseGemCost;
    public int baseGoldSellPrice;
    public int baseGemSellPrice;

    [Header("Recipe Data")]
    public RecipeData recipeData;
}

public enum ItemType
{
    Ressource,
    Equipment,
    Consummable,
    Recipe
}

public enum EquipmentType
{
    Head,
    Chest,
    Hands,
    Legs,
    Feet,
    Weapon
}
