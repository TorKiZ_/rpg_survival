﻿using System.Collections;
using UnityEngine;

public class ExplosiveMine : MonoBehaviour
{
   /* [Header("Particles")]
    [SerializeField] private GameObject explosiveParticle = null;
    [SerializeField] private Vector3 explosionOffset = new Vector3(0, 1, 0);*/

    [SerializeField] private GameObject childrenParticleSystem = null;
    [SerializeField] private ParticleSystem particleSystem = null;

    [Header("Audio Effect")]
    [SerializeField] private AudioSource bombAudioSource = null;

    [Header("Damages")]
    [SerializeField] private float damageDealt = 0;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine(ParticlesSystem(2f));
            //GameObject particle = Instantiate(explosiveParticle, transform.position + explosionOffset, Quaternion.identity);
            // maleGruntAudioSource.Play();
            // Destroy(particle, 2);
        }
    }

    IEnumerator ParticlesSystem(float seconds)
    {
        bool toPlayOneTime = true;
        while (toPlayOneTime)
        {
            childrenParticleSystem.SetActive(true);
            particleSystem.Play();
            bombAudioSource.Play();
            PlayerStats.instance.TakeDamage(damageDealt);
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider>().enabled = false;
            yield return new WaitForSeconds(seconds);
            particleSystem.Stop();
            childrenParticleSystem.SetActive(false);
            Destroy(gameObject);
            toPlayOneTime= false;
        }
    }
}
